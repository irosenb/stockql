<?
	
	require_once("includes/helpers.php");
	session_start();

	$dbh = connect_db('mysql:host=localhost;dbname=project1', 'jharvard', 'crimson');
	
	if (!isset($_POST['quantity'])) { //go back if incomplete
		redirect('results');
	}	 

	if (!isset($_SESSION['email'])) {
		redirect('index');
	}
	elseif (!isset($_SESSION['row']) || !isset($_SESSION['q'])) {
		redirect('stocks');
	}

	$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$_SESSION['q']}&f=sl1d1t1c1ohgv&e=.csv";
	$handle = fopen($url, "r");

	$row = fgetcsv($handle);

	if ($row[1] == 0.00) {
		redirect('stocks');
	}

	fclose($handle);

	$amount = htmlspecialchars($_POST['quantity']);
	$value = htmlspecialchars($_SESSION['row']);
	$email = htmlspecialchars($_SESSION['email']);
	$name = htmlspecialchars($_SESSION['q']);	

	if (!filter_var($amount, FILTER_VALIDATE_INT) || $amount < 0) {
		redirect('results');
	}

	$dbh->beginTransaction();

	foreach($dbh->query("SELECT	id,balance FROM	users WHERE email='$email'") as	$row) {
		$id = $row['id']; //from section code
		$bal = $row['balance'];
		if ($bal < $amount * $value) {
			redirect ('results');
		} 

		$bal = $bal - $amount * $value;
		
		$dbh->exec("UPDATE users SET balance = '$bal' WHERE id ='$id'");
	}
	unset($row);

	$row = $dbh->query("SELECT shares, price FROM stocks WHERE id ='$id' AND name='$name'");

	$result = $dbh->query("SELECT name FROM stocks WHERE id = '$id' AND name = '$name'");

	if ($result->rowCount() <= 0) {

		$dbh->exec("INSERT INTO stocks (name, shares, id, price, indvprc) VALUES ('$name', '$amount', '$id', '$value', '$value')");

	}
	else {
		foreach($dbh->query("SELECT shares, price FROM stocks WHERE id ='$id' AND name='$name'") as	$row) {

			$shares = $row['shares'] + $amount; //preparing to add on to existing stock
			$total = $row['price'] * $amount + $value;
			$dbh->exec("UPDATE stocks SET shares = '$shares', price = '$total', indvprc = '$value' WHERE id ='$id' AND name='$name'");	
		}
	}
	
	
	
	$dbh->commit();
	redirect('portfolio');

?>