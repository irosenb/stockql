	function check(){

	if ($('#name').text() == "") {
	
	alert("Please fill in the name form.");
	return false;
	} else if ($['#email'].text() == "") {

		alert("Please provide an email.");
		return false;
	} else if (!$['#email'].match("([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})")) { //got email regex from stackoverflow
	
		alert("Please provide valid email");
		return false;
	} else if ($['#password1'].text() == "") {
	
		alert("Please provide password.");
		return false;
	} else if ($['#password1'].length() < 6) {
	
		alert("Password must be more than six characters.");
		return false;
	} else if (!$['#password1'].match("[A-Za-z].*[0-9]|[0-9].*[A-Za-z]")) {
	
		alert("Password must have letters and numbers.");
		return false;
	} else if ($['#password1'].text() != ['#password2'].text()) {
	
		alert ("The passwords do not match.");
		return false;
	}
