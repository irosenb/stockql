<? 
	session_start();
	require("includes/helpers.php"); //all just setting up

	$dbh = connect_db('mysql:host=localhost;dbname=project1', 'jharvard', 'crimson');

	if (!isset($_SESSION['email'])) {
		redirect('index');
	}

	if (!isset($_POST['sell']) || !isset($_POST['name']) || empty($_POST['name'])) {
		redirect('portfolio');
	}
	//security measures here
	$name = htmlspecialchars($_POST['name']); 
	$amount = htmlspecialchars($_POST['sell']);
	$email = htmlspecialchars($_SESSION['email']);
	//end security

	foreach($dbh->query("SELECT	id, balance FROM users WHERE email='$email'") as $row) { //getting balance and id
		$id = $row['id']; //from section code
		$bal = $row['balance'];
	}
	unset($row);

	foreach ($dbh->query("SELECT shares, price FROM stocks WHERE id='$id' AND name='$name' ") as $row) { //getting variables for shares and prices
		$shares = $row['shares'];
		$price = $row['price'];
	}
	unset($row);
	
	if ($shares - $amount == 0) { //if sell all stocks, value must be preserved. 
		$org_shares= $shares; //creating variable store shares 
	}

	render('header', array('title' => 'StockQL Stocks'));
	//end setup

	if (!filter_var($amount, FILTER_VALIDATE_INT) || $amount < 1 || $amount > $shares) { //amount validation
		redirect('results');
	}
	$shares = $shares - $amount;

	
	$dbh->beginTransaction();
	if ($shares == 0) { //deleting or updating the shares 
		$dbh->exec("DELETE FROM stocks WHERE id='$id' AND name='$name'");
	}
	else {
		$dbh->exec("UPDATE stocks SET shares = '$shares' WHERE id='$id' AND name='$name'");		
	}

	$s = urlencode($name); 
	$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$s}&f=sl1d1t1c1ohgv&e=.csv";
	$handle = fopen($url, "r");
	$curprice = price($handle);
	if ($share == 0) {
		$bal = $bal + $org_shares * $curprice;
	}
	else {
		$bal = $bal + $shares * $curprice;
	}
	

	$dbh->exec("UPDATE users SET balance = '$bal' WHERE email='$email'");
	$dbh->commit();

	redirect('portfolio');
?>