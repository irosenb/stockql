<?php

require_once('includes/helpers.php');
session_start();

render('header', array('title' => 'StockQL Index')); 

?>

<script type="text/javascript">
	$(document).ready(function(){
		
		$("#email").focus();

	})

	function check() {

		if ($("#email").val() == "") {

			alert("Please provide an email.");
			return false;
		} else if (!$("#email").val().match(/.+@.+\./)) { 
		
			alert("Please provide valid email");
			return false;
		} else if ($("#password1").val().length < 6) {
		
			alert("Password must be more than six characters.");
			return false;
		} else if (!$("#password1").val().match("[A-Za-z].*[0-9]|[0-9].*[A-Za-z]")) {
		
			alert("Password must have letters and numbers.");
			return false;
		} else if ($("#password1").val() != $("#password2").val()) {
		
			alert("The passwords do not match.");
			return false;
		}

		return true;
	}

	function checkLogin() {
		if ($("#log_email").val() == "") {
			
			alert("Please provide an email.");
			return false;
		} else if ($("#log_password").val() == "") {

			alert("Please provide a password.");
			return false;
		}

		return true; 
	}

	function hover (id, end[]) {
		
		if (end.indexOf(id) < -1) {
			return;
		}
		else {
			$(id).focus();
		}

		end.push(id);
	}
</script>

<?php render('header2') ?>


<form action="register.php" method="post" id="register" name="register" class="well form-horizontal" onsubmit="return check();">
	<label>Email:</label><input type="text" name="email" id="email" />
	<label>Password:</label><input type="password" name="password1" id="password1" />
	<label>Repeat Password:</label><input type="password" name="password2" id="password2" />
	<input type="submit" class="btn" value="Submit" />
</form>

<h1>OR</h1> <br/>

<h2>Sign In</h2>
<form action="login.php" method="post" name="login" id="login" class="well form-horizontal" onsubmit="return checkLogin();" onmouseover="hover('#log_email');">
	<label>Email:</label><input type="text" name="email" id="log_email" />
	<label>Password:</label><input type="password" name="password" id="log_password" />
	<input type="submit" class="btn" />
</form>



<?php render('footer'); ?>
