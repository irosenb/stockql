<? 
	session_start();
	require("includes/helpers.php");

	$dbh = connect_db('mysql:host=localhost;dbname=project1', 'jharvard', 'crimson');

	if (!isset($_SESSION['email'])) {
		redirect('index');
	}
	
	$email = htmlspecialchars($_SESSION['email']);

	foreach($dbh->query("SELECT	id, balance FROM users WHERE email='$email'") as $row) {
		$id = $row['id']; //from section code
		$bal = $row['balance'];
		
	}
	unset($row);

	render('header', array('title' => 'StockQL Stocks'));

?>
<script type="text/javascript">
	function is_int(value){ 
	  
	  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){ //got this online, checks if int
	      
	      return true;
	  } else { 
	      
	      return false;
	  } 
	}

	function validate(sell) {
		if (($(sell).val() < 1) || ($(sell).val() == "")) {
			
			alert("You must enter more than one item.");
			return false;
		}

		var x = $(sell).val();

		if (!is_int(x)) {
			alert("Integers only.");
			return false;
		}

		return true;
	}
</script>

<?php

 render('header2'); 

?>
<h1><?= $email ?> </h1>
<h1>Balance: <?= $bal ?></h1>
<h1>Portfolio:</h1>

	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Symbol</th>
				<th>Shares</th>
				<th>Current Price</th>
				<th>Change</th>
				<th>Sell</th>
			</tr>
		</thead>
		<tbody>
			<?
			$i = 1;
			foreach($dbh->query("SELECT	name, shares, price, indvprc FROM stocks WHERE id='$id'") as $row) {
				$name = $row['name']; //from section code
				$s = urlencode($name);

				$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$s}&f=sl1d1t1c1ohgv&e=.csv";
				$handle = fopen($url, "r");
				$curprice = price($handle); //current price
				$shares = $row['shares'];
				$price = $row['indvprc'];
				$diff = $curprice - $price;
				 //print out names,prices,etc
				print '<tr>
					   	<td>' . $name .'</td>
					   		<td>' . $shares . '</td>
					   		<td>' . $curprice . '</td>
					   		<td>' . $diff . '</td>
					   		<td>
					   			<form action="subtract.php" name="subtr' . $i .'" method="post" class="form-search" onsubmit="return validate(\'#sell' . $i . '\');">
					   				<input type="hidden" name="name" id="name" value="' . $name .'">
									<input type="text" name="sell" id="sell' . $i . '" style="width: 100px;">
									<input type="submit" class="btn" value="Sell">
					   			</form>
					   	</td>
					   </tr>';

				$i++; //end printing prices, etc
			}
			?>
		</tbody>
	</table>

