<?

	session_start();
	require_once("includes/helpers.php"); 
	
	if (!isset($_SESSION['email'])) {
		redirect('index');
	}

	if (!isset($_GET['search'])) {
		redirect('stocks');
	}

	$s = urlencode($_GET['search']); //downloading and parsing csv file. code taken from lecture5
	
	$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$s}&f=sl1d1t1c1ohgv&e=.csv";
	$handle = fopen($url, "r");

	$row = fgetcsv($handle);
	
	if ($row[1] == 0.00) {
		redirect('stocks');
	}

	fclose($handle);

	$_SESSION['row'] = $row[1]; //store rows for later use
	$s = htmlspecialchars($_GET['search']);

	$_SESSION['q'] = $s;

	render('header', array('title' => 'StockQL Stocks'));

?>

<script type="text/javascript">
	function is_int(value){ 
	  
	  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){ //got this online, checks if int
	      
	      return true;
	  } else { 
	      
	      return false;
	  } 
	}
	function validate() {
		if (($('#quantity').val() < 1) || ($('#quantity').val() == "")) {
			
			alert("You must enter more than one item.");
			return false;
		}

		var x = $('#quantity').val();

		if (!is_int(x)) {
			alert("Integers only.");
			return false;
		}

		return true;
	}
	
</script>

<?php

 render('header2'); 

?>

<h1><?= $s ?>.</h1>

<h2> Price: <?=	$row[1] ?></h2>

<br>
<form action="modify.php" method="post" class="form form-inline" onsubmit="return validate();">
	<input type="text" name="quantity" class="input-min" id="quantity">
	<input type="submit" class="btn" value="Buy"> 
</form>

<? 
	render('footer'); 
?>