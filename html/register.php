<?
	require_once('includes/helpers.php');

	$dbh = connect_db('mysql:host=localhost;dbname=project1', 'jharvard', 'crimson');

	session_start();

	if (!isset($_POST['email']) || !isset($_POST['password1']) || !isset($_POST['password2'])) { //is everything present?
		redirect('index');
	}

	$password1 = htmlspecialchars($_POST['password1']); //get rid of malicious code
	$email = htmlspecialchars($_POST['email']);
	$password2 = htmlspecialchars($_POST['password2']);	


	if (empty($email)) { //server-side checking
		redirect('index');

	} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) { //real email?
		redirect('index');

	} elseif (strlen($password1) < 6) { //password not required length
		redirect('index');

	}  elseif (!(preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $password1))) { //numbers and letters?
		redirect('index');

	} elseif ($password1 != $password2) { //same passwords?
		redirect('index');
	}
	
	$password1 = md5($password1);
	
	$dbh->beginTransaction(); //lock the fridge	
	$dbh->exec("INSERT INTO users (email, password) VALUES ('$email', '$password1')"); //putting email + password into db
	$dbh->commit(); //unlocked

	$_SESSION['email'] = $email;

	redirect('stocks'); //go to stocks page

?>